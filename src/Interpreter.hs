{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TemplateHaskell  #-}

module Interpreter where

import           Control.Lens        (view)
import           Control.Monad.State
import           Data.Map            (Map)
import qualified Data.Map            as Map

import           AST
import           Env
import           Error
import           Typechecker

-- runs a program, result type is list of strings describing execution of statements
runProg :: Prog -> State Env [String]
runProg = (filter (not . null) <$>) . mapM processStmt
    where
        processStmt s = do
            s' <- runStmt s
            case s' of
                (Left e) -> do
                    modify $ addError e
                    pure ""
                Right s  -> pure s

-- adds a constant or definition to the env
runStmt :: Stmt -> State Env (Either Error String)
runStmt (Const name constType) = do
    env <- get
    if isNameCollision env name
        then pure $ Left $ LookupError $ "got name collision when attempting to add constant " ++ name
        else do
            modify (addJudgement (name, constType))
            pure $ Right ""

runStmt def@(Def name defType body) = do
    env <- get
    runDef env $ do
        validatedDef <- if not $ isNameCollision env name
            then Right def
            else Left $ LookupError $ "got name collision when attempting to add definition " ++ name
        bodyType     <- getBodyType env validatedDef
        checkWellTyped env validatedDef bodyType

-- check if their is a name collision with an existing constant or def
isNameCollision :: Env -> String -> Bool
isNameCollision env name = Map.member name (view globalContext env) || Map.member name (view defs env)

-- gets the type of the body of a definition
getBodyType :: Env -> Stmt -> Either Error Term
getBodyType env (Def _ _ body) = typeOf env body

-- checks if the type of the body of a defintion matches its prescribed type
checkWellTyped :: Env -> Stmt -> Term -> Either Error Stmt
checkWellTyped env def@(Def _ expected _) bodyType = do
    if expected == bodyType
        then Right def
        else Left $ TypeError $ "definition " ++ show def ++ " does not typecheck:\n"
            ++ "\texpected " ++ show expected ++ "\n\tbut got " ++ show bodyType

-- adds a definition to the environment and appends to the list of results
runDef :: Env -> Either Error Stmt -> State Env (Either Error String)
runDef env = mapM execDef
    where
        execDef (Def name defType _) = do
            modify (addJudgement (name, defType))
            modify (addDef  (name, defType))
            pure $ name ++ " typechecks"
