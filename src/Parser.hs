{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
{-# LANGUAGE TupleSections    #-}

module Parser where

import Data.List

import Text.Parsec
import Text.Parsec.Expr

import AST

type Parser a = forall s st m . Stream s m Char => ParsecT s st m a

parseProg :: Parser ProgP
parseProg = skipMany whitespace >> many parseStmt <* eof

parseStmt :: Parser StmtP
parseStmt = parseConst <|> parseDef

parseConstStart :: Parser String
parseConstStart = lexeme $ string "constant" <|> string "axiom"

parseConst :: Parser StmtP
parseConst = do
    parseConstStart
    name <- lexeme identifier
    hasType
    Const name <$> parseExpr

parseDefStart :: Parser String
parseDefStart = lexeme $ string "def" <|> string "theorem"

parseDef :: Parser StmtP
parseDef = do
    parseDefStart
    name <- lexeme identifier
    hasType
    defType <- parseExpr
    symbol $ string ":="
    Def name defType <$> parseExpr

parseExpr :: Parser TermP
parseExpr = tryFrom [ parseSort,
                      parseProp,
                      parseLambda,
                      parseForall,
                      parseFunctionType,
                      parseApp,
                      parseVar,
                      parseParenthetical
                    ]

parseSort :: Parser TermP
parseSort = do
    lexeme $ string "Sort"
    n <- read <$> lexeme (many1 digit)
    pure $ Sort n

parseProp :: Parser TermP
parseProp = lexeme $ string "Prop" >> pure P

parseHandle :: Parser Handle
parseHandle = try (Named <$> identifier) <|>
    try (string "_" >> pure Wildcard)

parseLambda :: Parser TermP
parseLambda = do
    symbol (string "\\" <|> string "λ")
    handles <- many1 (lexeme parseHandle)
    hasType
    argType <- parseExpr
    bodySep
    body <- parseExpr
    pure $ foldr (Lambda . (, argType)) body handles

parseForall :: Parser TermP
parseForall = do
    symbol (string "@" <|> string "∀")
    handles <- many1 (lexeme parseHandle)
    hasType
    argType <- parseExpr
    bodySep
    body <- parseExpr
    pure $ foldr (Forall . (, argType)) body handles

parseFunctionType :: Parser TermP
parseFunctionType = do
    argType <- parseArgType
    symbol $ string "->"
    Forall (Wildcard, argType) <$> parseExpr
    where
        -- disallow functionType combinator in arg to prevent infinite loop
        parseArgType = tryFrom [ parseSort,
                                 parseProp,
                                 parseLambda,
                                 parseForall,
                                 parseApp,
                                 parseVar,
                                 parseParenthetical
                               ]

parseApp :: Parser TermP
parseApp = do
    f <- parseVar <|> parseParenthetical
    foldl' curryArgs f <$> many1 parseArg
    where
        curryArgs f arg = App f arg
        -- disallow combinators with spaces (sort and app) as arguments
        parseArg = tryFrom [ parseProp,
                             parseLambda,
                             parseForall,
                             parseFunctionType,
                             parseVar,
                             parseParenthetical
                           ]

parseVar :: Parser TermP
parseVar = Variable <$> lexeme identifier

parseParenthetical :: Parser TermP
parseParenthetical = between (symbol (string "(")) (symbol (string ")")) parseExpr

hasType :: Parser ()
hasType = symbol (string ":") >> pure ()

bodySep :: Parser ()
bodySep = symbol (string ".") >> pure ()

identifierChar :: Parser Char
identifierChar = alphaNum <|> char '_'

identifier :: Parser String
identifier = do
    try $ notFollowedBy $ try parseConstStart <|> try parseDefStart
    c  <- letter <|> char '_'
    cs <- many identifierChar
    pure (c : cs)

whitespace :: Parser ()
whitespace = space >> pure ()

-- Note: we use the full type signature instead of Parser for these combinators
-- otherwise the type variables aren't shared (plus issues with impredicative polymorphism)

lexeme :: Stream s m Char => ParsecT s st m a -> ParsecT s st m a
lexeme parser = do
    p <- parser
    -- this is a bit of a hack to avoid having to do proper tokenization
    skipMany1 whitespace <|> notFollowedBy identifierChar
    pure p

symbol :: Stream s m Char => ParsecT s st m a -> ParsecT s st m a
symbol parser = do
    p <- parser
    skipMany whitespace
    pure p

tryFrom :: Stream s m Char => [ParsecT s st m a] -> ParsecT s st m a
tryFrom [] = error "must supply at least one parser to try"
tryFrom parsers = foldl' (<|>) p ps
    where (p:ps) = map try parsers
