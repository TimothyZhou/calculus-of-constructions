{-# LANGUAGE TemplateHaskell #-}

module Preprocessor where

import           Control.Lens (makeLenses, over, view)
import           Data.Map     (Map)
import qualified Data.Map     as Map

import           AST

-- wrapper for a multimap between names and indices
data IndexLookup = IndexLookup {
    _indices :: Map String [Int],
    _size    :: Int
} deriving (Eq, Show)
makeLenses ''IndexLookup

-- helper function to add a handle to the index lookup
append :: Handle -> IndexLookup -> IndexLookup
append Wildcard indexLookup = over size succ indexLookup
append (Named name) indexLookup = over size succ $ over indices (appendIndex name) indexLookup
    where
        appendIndex name indexMap = if not $ Map.member name indexMap
            then Map.insert name [nextIndex] indexMap
            else Map.adjust (nextIndex:) name indexMap
        nextIndex = -(view size indexLookup) - 1

-- looks up an index given a name
getIndex :: String -> IndexLookup -> Maybe Int
getIndex name indexLookup = (view size indexLookup +) . head <$> Map.lookup name (view indices indexLookup)

-- converts a program to de'brujin indices
preprocess :: ProgP -> Prog
preprocess = map preprocessStmt

-- converts a Stmt to de'brujin indices
preprocessStmt :: StmtP -> Stmt
preprocessStmt (Const name constType)  = Const name $ preprocessTerm constType
preprocessStmt (Def name defType body) = Def name (preprocessTerm defType) (preprocessTerm body)

-- wrapper for preprocessTerm'
preprocessTerm :: TermP -> Term
preprocessTerm = preprocessTerm' IndexLookup {_indices = Map.empty, _size = 0}

-- converts a term to de'brujin indices using index lookup
preprocessTerm' :: IndexLookup -> TermP -> Term
preprocessTerm' _ (Sort n) = Sort n
preprocessTerm' indexLookup (Lambda (handle, boundType) body) = Lambda processedBinding processedBody
    where
        processedBinding = (handle, preprocessTerm' indexLookup boundType)
        processedBody = preprocessTerm' (append handle indexLookup) body

preprocessTerm' indexLookup (Forall (handle, boundType) body) = Forall processedBinding processedBody
    where
        processedBinding = (handle, preprocessTerm' indexLookup boundType)
        processedBody = preprocessTerm' (append handle indexLookup) body

preprocessTerm' indexLookup (App f arg) = App (preprocessTerm' indexLookup f) (preprocessTerm' indexLookup arg)
preprocessTerm' indexLookup (Variable name) = maybe (Variable $ Free name) (Variable . Bound) (getIndex name indexLookup)
