{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies    #-}

module Env where

import           Control.Lens (makeLenses, over, view)
import           Data.Map     (Map)
import qualified Data.Map     as Map

import           AST
import           Error

type Judgement = (String, Term)

data Env = Env {
    _localContext  :: [Binding],
    _globalContext :: Map String Term,
    _defs          :: Map String Term,
    _errors        :: [Error]
} deriving (Eq, Show)
makeLenses ''Env

newEnv :: Env
newEnv = Env {
    _localContext = [],
    _globalContext = Map.empty,
    _defs    = Map.empty,
    _errors  = []
}

-- adds a type to the current local context
addBinding :: Binding -> Env -> Env
addBinding b = shiftIndices . addBinding' b
    where
        addBinding' (Wildcard, _) = id
        addBinding' b             = over localContext (b:)
        shiftIndices = over localContext (map shiftVar)
        shiftVar (handle, Variable (Bound i)) = (handle, Variable (Bound $ succ i))
        shiftVar t = t

-- gets a binding from the current local context
getBinding :: Var -> Env -> Binding
getBinding (Bound i) env = view localContext env !! i

-- adds a binding to the global context
addJudgement :: Judgement -> Env -> Env
addJudgement = over globalContext . uncurry Map.insert

-- adds a definition to the current Env
addDef :: Judgement -> Env -> Env
addDef = over defs . uncurry Map.insert

-- adds an error to the current Env
addError :: Error -> Env -> Env
addError = over errors . (:)

-- substitutes a term into a free or bound variable
substitute :: Var -> Term -> Term -> Term
substitute var@(Free _) val t = case t of
    Sort n             -> t
    Variable otherVar  -> if var == otherVar then val else t
    Lambda arg ret     -> Lambda (replaceTerm <$> arg) (replaceTerm ret)
    Forall arg retType -> Forall (replaceTerm <$> arg) (replaceTerm retType)
    App f arg          -> App (replaceTerm f) (replaceTerm arg)
    where
        replaceTerm = substitute var val

substitute var val t = case t of
    Sort n                -> t
    Variable (Free _)     -> t
    v@(Variable otherVar) -> if var == otherVar then val else v
    Lambda arg ret        -> Lambda (replaceTerm <$> arg) (replaceTerm ret)
    Forall arg retType    -> Forall (replaceTerm <$> arg) (replaceTerm retType)
    App f arg             -> App (replaceTerm f) (replaceTerm arg)
    where
        incIndex (Bound i) = Bound $ succ i
        replaceTerm u = case u of
            Lambda _ _ -> substitute (incIndex var) val u
            Forall _ _ -> substitute (incIndex var) val u
            _          -> substitute var val u
