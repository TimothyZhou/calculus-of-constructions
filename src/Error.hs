{-# LANGUAGE TemplateHaskell #-}

module Error where

data Error = LookupError String
           | TypeError String
    deriving Eq

instance Show Error where
    show (LookupError msg) = "LookupError: " ++ msg
    show (TypeError msg) = "TypeError: " ++ msg
