{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Typechecker where

import           Control.Lens (view)
import           Data.Map     (Map)
import qualified Data.Map     as Map

import           AST
import           Env
import           Error

-- gets the type of a term
typeOf :: Env -> Term -> Either Error Term
typeOf _   (Sort n) = Right $ Sort (succ n)
typeOf env (Variable v) = lookupVar v env
    where
        lookupVar (Free name) env = maybe (lookupError name) Right (Map.lookup name (view globalContext env))
        lookupVar v env  = Right $ snd $ getBinding v env
        lookupError name = Left $ LookupError $ "undefined variable " ++ name

typeOf env (Forall binding body) = do
    l1 <- typeOf (addBinding binding env) body
    l2 <- typeOf env (snd binding)
    maxUniverse l1 l2
    where
        maxUniverse P _               = Right P
        maxUniverse (Sort n) (Sort m) = Right $ Sort $ max m n
        maxUniverse a b               = case a of
            Sort _ -> Left $ TypeError $ "term has type " ++ show a ++ " but expected Sort n"
            _      -> Left $ TypeError $ "term has type " ++ show b ++ " but expected Sort n"

typeOf env (Lambda binding body) = do
    typeOf env (snd binding)
    retType <- typeOf (addBinding binding env) body
    Right $ Forall binding retType

typeOf env (App f arg) = do
    fType   <- typeOf env f
    argType <- typeOf env arg
    case fType of
        Forall (_, boundType) retType -> if boundType == argType
            then Right $ substitute (Bound 0) arg retType
            else Left $ TypeError $ "function " ++ show f ++ " expects an argument of type " ++
                show boundType ++ " but got " ++ show arg ++ " with type " ++ show argType
        _ -> Left $ TypeError $ "attempted to apply non-function " ++ show f ++
            " to term " ++ show arg
