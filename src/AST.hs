{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE EmptyDataDecls #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module AST where

-- TTG phases
data Parse
data Typecheck

data Var = Free String
         | Bound Int
    deriving (Eq, Show)

type family Ref p where
    Ref Parse = String
    Ref Typecheck = Var

data Handle = Wildcard
            | Named String
    deriving (Eq, Show)

type BindingX p = (Handle, TermX p)

instance {-# OVERLAPPING #-} Eq (Ref p) => Eq (BindingX p) where
    (_, t1) == (_, t2) = t1 == t2

-- TODO: Need to reduce terms to beta-eta normal form when checking for equality
-- TODO: Pretty printing for terms (ignore bindings for pi types with constant codomains)
data TermX p = Sort Integer
             | Lambda (BindingX p) (TermX p)
             | Forall (BindingX p) (TermX p)
             | App (TermX p) (TermX p)
             | Variable (Ref p)

pattern P = Sort 0
pattern T = Sort 1

data StmtX p = Const String (TermX p)
             | Def String (TermX p) (TermX p)

type BindingP = BindingX Parse
type Binding = BindingX Typecheck

type StmtP = StmtX Parse
type Stmt = StmtX Typecheck

type TermP = TermX Parse
type Term = TermX Typecheck

type ProgP = [StmtX Parse]
type Prog = [StmtX Typecheck]

deriving instance Eq (Ref phase) => Eq (TermX phase)
deriving instance Show (Ref phase) => Show (TermX phase)

deriving instance Eq (Ref phase) => Eq (StmtX phase)
deriving instance Show (Ref phase) => Show (StmtX phase)
