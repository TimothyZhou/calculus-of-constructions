About
=====
This is a minimalist implementation in Haskell of an interpreter for the [calculus of constructions](https://en.wikipedia.org/wiki/Calculus_of_constructions), a dependent type theory which is the foundation for modern proof assistants like Coq and Lean.
## Syntax
Here is a simplified version of the grammar:
```
term = "Prop"
     | "Sort" digit
     | ("λ" | "\") vars ":" term "." term
     | ("@" | "∀") vars ":" term "." term
     | term term
     | var
constant = ("constant" | "axiom") var ":" term
theorem = ("def" | "theorem") var ":" term ":=" term
stmt = constant | theorem
```
Some features:
- Conversion to de Brujin indices
- Infinite hierarchy of types
- Impredicative `Prop`
## Example
Here is a verified proof of `p` implies `q` implies `p`
```
theorem t1 : @p q : Prop. p -> q -> p := \p q : Prop. \hp : p. \hq : q. hp
```
```
t1 typechecks
```
## References Used
1) Coquand, Thierry; Huet, Gérard (1988). "The Calculus of Constructions" (PDF). Information and Computation. 76 (2–3): 95–120. doi:10.1016/0890-5401(88)90005-3.
2) https://en.wikipedia.org/wiki/Calculus_of_constructions
3) https://leanprover.github.io/theorem_proving_in_lean/
