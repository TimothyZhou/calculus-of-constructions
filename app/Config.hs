module Config where

import Options.Applicative as Opt

data Config = Config {
    file  :: Maybe FilePath,
    debug :: Bool
} deriving (Eq, Show)

parseConfig :: Opt.Parser Config
parseConfig = Config
    <$> optional (argument str (metavar "FILE" <> help "File to run. Defaults to input from stdin."))
    <*> switch (long "debug" <> short 'd' <> help "Whether debug mode is enabled.")

getArgs = execParser opts
    where
        opts = info (parseConfig <**> helper) descriptions
        descriptions = fullDesc <>
            progDesc "A calculus of constructions interpreter."
            <> header "coc"
