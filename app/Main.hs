{-# LANGUAGE TemplateHaskell #-}

module Main where

import Control.Monad.State
import Text.Parsec         (parse)
import Control.Lens        (view)

import System.IO (hPutStrLn, stderr)
import System.Exit

import AST
import Env
import Parser
import Preprocessor
import Interpreter

import Config

main :: IO ()
main = getArgs >>= runFile

putError = hPutStrLn stderr

-- interprets a file
runFile :: Config -> IO ()
runFile cfg = do
    source <- processConfig cfg
    case parse parseProg "" source of
        Left err -> error $ show err
        Right progP -> do
            let prog = preprocess progP
            let (results, env) = runState (runProg prog) newEnv
            if not $ null $ view errors env
                then do
                    mapM_ (putError . show) (view errors env)
                    exitFailure
                else mapM_ putStrLn results

-- gets source from config (either stdin or from file)
processConfig :: Config -> IO String
processConfig cfg = maybe getContents readFile (file cfg)
